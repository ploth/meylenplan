import re


class Meal(object):
    def __init__(self, name, allergens, potentially_not_a_meal=False):
        self.name = name
        self.allergens = allergens
        self.price = None
        self.potentially_not_a_meal = potentially_not_a_meal
        self.__parseMetaData()

    def __parseMetaData(self):
        salad_pattern = re.compile(r'.*\ssalat\s.*', flags=re.IGNORECASE)
        match = re.match(salad_pattern, self.name)
        self.extraSalad = True if match else False
        vegan_pattern = re.compile(r'.*vegan.*', flags=re.IGNORECASE)
        match = re.match(vegan_pattern, self.name)
        self.isVegan = True if match else False
        soup_pattern = re.compile(r'.*\ssuppe\s.*', flags=re.IGNORECASE)
        match = re.match(soup_pattern, self.name)
        self.isSoup = True if match else False
