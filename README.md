# meylenplan

Fetches and displays the SchlemmerMeyle meal plan for today.

## Installation and Update

```sh
pip install --user --upgrade meylenplan
```

## Usage

Invoke `meylenplan` in a terminal.

## Notice of Non-Affiliation and Disclaimer

We are not affiliated, associated, authorized, endorsed by, or in any way
officially connected with SchlemmerMeyle, or any of its subsidiaries or its
affiliates. The official SchlemmerMeyle website can be found at
http://schlemmermeyle.de/. The name “SchlemmerMeyle” as well as related names,
marks, emblems and images are registered trademarks of SchlemmerMeyle. 
